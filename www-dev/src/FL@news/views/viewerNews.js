/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "marionette", "FL@news/config"], function (Hybreed, Marionette, Config) {

    "use strict";

    var ViewerNewsView = Marionette.ItemView.extend({

        template: _.template(Config.html_viewerNews),

        tagName: "div",

        className: "viewerNews",

        ui: {
            back: ".iconBackBlack",
            title: ".titleNews",
            optBookmark: "#optBookmark",
            contentViewerNews: ".contentViewerNews"
        },

        modelEvents: {
            "change:bookmark": "refreshBookmark"
        },

        events: {},

        attributes: function () {
            return {
                "data-bookmark": this.model.get('bookmark')
            }
        },

        triggers: {
            "click @ui.back": "viewerNews:click:back",
            "click @ui.optBookmark": "viewerNews:click:bookmark"
        },

        initialize: function (options) {
            this.scroll = null;
            this.strings = options.strings;
        },

        serializeData: function () {
            return {
                strings: this.strings,
                news: this.model.toJSON()
            };
        },

        onShow: function () {

            this.scroll = Hybreed.UI.generateScrollProbe(this.ui.contentViewerNews[0], null);

            var sizeTitle = 2;

            while (this.ui.title[0].scrollWidth > this.ui.title.outerWidth(true)) {
                sizeTitle = sizeTitle - 0.1;
                this.ui.title.css("font-size", sizeTitle + "em");
            }

            Hybreed.UI.hideSpinner();
        },

        onDestroy: function () {

            if (!_.isNull(this.scroll)) {
                this.scroll.destroy();
                this.scroll = null;
            }
        },

        /**
         * -------------------------------------------------------------- ACTIONS
         **/
        refreshBookmark: function () {
            this.$el.attr("data-bookmark", this.model.get('bookmark'))
        }

    });

    return ViewerNewsView;

});
