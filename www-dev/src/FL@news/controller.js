/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "broker",
        "FL_GV@genericsViews/layoutTabs/layoutTabs",
        "FL_GV@genericsViews/listInfinite/listInfinite",
        "FL@news/views/viewerNews",
        "text!data/news.json",
        "text!data/contentNew.html",
        "text!data/translations.json"],
    function (Hybreed, Broker, LayoutNews, ListNews, ViewerNewsView, DataNews, ContentNew, Strings) {

        "use strict";

        var API,
            PrivateAPI;

        // VIEWS
        var layoutMenuLeftView = null,
            layoutNews = null,
            viewerNewsView = null,
            listNewsView = null;

        // DATA
        var collectionNews = null,
            contentNewModel = null,
            strings = JSON.parse(Strings);

        // OTHERS
        /*
         *  - command: para volver a un modulo exterior que invocó al visor de noticias
         *  - section: pestaña desde que se lanza el visor de noticias
         *  - data: información útil para volver, por ejemplo scrollTo
         * */
        var infoToBack = {};

        // HARD-CODE
        var newsArray = JSON.parse(DataNews);

        /**
         *     *************  PUBLIC API  ************
         */

        API = {

            startNews: function (back) {

                infoToBack = back;

                if (_.isNull(layoutMenuLeftView) || layoutMenuLeftView.isDestroyed) {
                    layoutMenuLeftView = Broker.reqres.request("FL@menuLeft:generateMenuLeft");
                    Broker.showViewInContent(layoutMenuLeftView, "content");
                }

                layoutNews = new LayoutNews({
                    title: strings["NEWS_TITLE"],
                    tabs: [
                        {
                            id: "tabLastNews",
                            title: strings["NEWS_LAST_NEWS"]
                        },
                        {
                            id: "tabsLG1",
                            title: strings["NEWS_LEGAL_AREA_1"]
                        },
                        {
                            id: "tabsLG2",
                            title: strings["NEWS_LEGAL_AREA_2"]
                        }
                    ],
                    strings: strings,
                    optRight: true,
                    textClass: "selectedNews"
                });

                PrivateAPI.associatedEventsLayoutNews();

                Broker.showViewInLayout(layoutNews, layoutMenuLeftView.contentCenter);

                var section = (_.has(infoToBack, "section")) ? infoToBack.section : null;

                PrivateAPI.showListNews(section);

                if (!_.isNull(section)) {
                    layoutNews.selectTab(_.isEqual(section, "legal A1") ? "tabsLG1" : "tabsLG2");
                }

                Hybreed.UI.hideSpinner();
            },

            startReaderNews: function (newModel, back) {

                PrivateAPI.getContentNew(newModel);

                infoToBack = back;

                viewerNewsView = new ViewerNewsView({strings: strings, model: contentNewModel});

                if (_.isNull(layoutMenuLeftView) || layoutMenuLeftView.isDestroyed) {
                    layoutMenuLeftView = Broker.reqres.request("FL@menuLeft:generateMenuLeft");
                    Broker.showViewInContent(layoutMenuLeftView, "content");
                }

                PrivateAPI.associatedEventsViewerNews();

                Broker.showViewInLayout(viewerNewsView, layoutMenuLeftView.contentCenter);
            }

        };


        /**
         *     *************   PRIVATE API  ************
         */
        PrivateAPI = {

            /***************   START DATA  *************/

            getContentNew: function (newModel) {
                if (!newModel.has('content')) {
                    var $news = $(ContentNew);
                    newModel.set("content", $news.find("nieuws\\:volledig-bericht").html());
                }

                contentNewModel = newModel;
            },

            getListNews: function (filter) {
                var deferredValue = $.Deferred();

                if (_.isNull(collectionNews)) {
                    collectionNews = new Backbone.Collection(newsArray);
                }

                if (!_.isNull(filter)) {
                    deferredValue.resolve(new Backbone.Collection(collectionNews.where({"category": filter})));
                } else {
                    deferredValue.resolve(collectionNews);
                }

                return deferredValue.promise();
            },

            /***************   SHOW VIEWS  *************/

            showListNews: function (filter) {
                var that = this;

                this.getListNews(filter)
                    .then(function (collection) {

                        listNewsView = new ListNews({
                            collection: collection,
                            typeListTemplate: "itemNew",
                            scrollTo: (_.has(infoToBack, "data") && _.has(infoToBack.data, "scrollTo")) ? infoToBack.data.scrollTo : null
                        });

                        Broker.showViewInLayout(listNewsView, layoutNews.contentTab);

                        that.associatedEventsListNewsView();
                    });
            },


            /***************   EVENTS  *************/

            associatedEventsLayoutNews: function () {
                var that = this;

                layoutNews.on("layoutTab:click:tabLastNews", function () {
                    infoToBack = {section: null, scrollTo: null};
                    that.showListNews(null);
                });

                layoutNews.on("layoutTab:click:tabsLG1", function () {
                    infoToBack = {section: "legal A1", scrollTo: null};
                    that.showListNews("legal A1");
                });

                layoutNews.on("layoutTab:click:tabsLG2", function () {
                    infoToBack = {section: "legal A2", scrollTo: null};
                    that.showListNews("legal A2");
                });
            },

            associatedEventsListNewsView: function () {

                listNewsView.on("listInfinite:tap:bookmark", function (model) {
                    model.set("bookmark", !model.get("bookmark"));
                });

                listNewsView.on("listInfinite:tap:item", function (model) {
                    infoToBack.data = {scrollTo: model.id};
                    API.startReaderNews(model, infoToBack);
                });
            },

            associatedEventsViewerNews: function () {

                viewerNewsView.on("viewerNews:click:back", function () {
                    if (_.has(infoToBack, "command")) {
                        Broker.commands.execute(infoToBack.command, infoToBack.data);
                    } else {
                        API.startNews(infoToBack);
                    }
                });

                viewerNewsView.on("viewerNews:click:bookmark", function (view) {
                    view.model.set('bookmark', !view.model.get('bookmark'));
                });
            }
        };

        return API;

    });