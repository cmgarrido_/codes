/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
require.config({
    paths: {
        "FL@news": "src/FL@news"
        // @build (keep line)
    }
});

define(["broker", "FL@news/controller"], function (Broker, Controller) {

    "use strict";

    var API;

    Broker.commands.setHandler("FL@news:startNews", function () {
        API.startNews();
    });

    Broker.commands.setHandler("FL@news:star:viewerNews", function (data) {
        API.startReaderNews(data);
    });


    API = {

        startNews: function () {
            Controller.startNews({});
        },

        startReaderNews: function (data) {
            Controller.startReaderNews(data.model, data.back);
        }
    };
});