/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "marionette", "FL@login/config"], function (Hybreed, Marionette, Config) {

    "use strict";

    var LoginView = Marionette.ItemView.extend({

        template: _.template(Config.html_login),

        tagName: "div",

        className: "login",

        ui: {
            "username": "#username",
            "password": "#password",
            "formLogin": "#groupButtons",
            "btnLogin": "#btnLogin",
            "showHidePassword": ".showHidePassword",
            "forgotPassword": ".l_forgot",
            "back": ".iconBackBlack"
        },

        events: {
            "keypress @ui.username": "sendFocusPassword",
            "keypress @ui.password": "sendFocusButton",
            "submit @ui.formLogin": "launchForm",
            "tap @ui.showHidePassword": "showHidePassword"
        },

        triggers: {
            "tap @ui.back": "login:click:back",
            "tap @ui.forgotPassword": "login:click:forgotPassword"
        },

        initialize: function (options) {
            this.scroll = null;
            this.strings = options.strings;
        },

        serializeData: function () {
            return {
                strings: this.strings
            }
        },

        onShow: function () {

            this.scroll = Hybreed.UI.generateScrollAllPlatforms(this.el, {
                bounce: false,
                mouseWheel: true,
                probeType: 1,
                click: true,
                tap: true,
                preventDefaultException: {tagName: /^(INPUT|TEXTAREA|SELECT)$/}
            });

            this.initializeConfigurationKeyboard();

            Hybreed.UI.hideSpinner();
        },

        onDestroy: function () {
            if (Hybreed.mobile) {
                window.removeEventListener('native.keyboardshow', this.keyboardShowHandler);
                window.removeEventListener('native.keyboardhide', this.keyboardHideHandler);
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }

            if (!_.isNull(this.scroll)) {
                this.scroll.destroy();
                this.scroll = null;
            }
        },

        initializeConfigurationKeyboard: function () {
            if (Hybreed.mobile) {
                var that = this;

                this.keyboardShowHandler = function (e) {
                    that.scroll.disable();
                    that.$el.css('bottom', (e.keyboardHeight - 30) + "px");
                    that.scroll.refresh();
                    that.scroll.scrollToElement(document.activeElement, 0, true, true);
                };

                this.keyboardHideHandler = function () {
                    setTimeout(function () {
                        if (!cordova.plugins.Keyboard.isVisible) {
                            that.$el.css('bottom', "0px");
                            that.scroll.scrollTo(0, -100, 0);
                            that.scroll.enable();
                            that.scroll.refresh();
                        }
                    }, 300);
                };

                window.addEventListener('native.keyboardshow', this.keyboardShowHandler);
                window.addEventListener('native.keyboardhide', this.keyboardHideHandler);

                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            }
        },

        /**
         * -------------------------------------------------------------- ACTIONS
         **/
        launchForm: function (ev) {
            ev.preventDefault();
            this.launchLogin();
        },

        launchLogin: function () {
            Hybreed.UI.showSpinner();

            var error = "",
                data = {
                    username: this.ui.username.val().trim(),
                    password: this.ui.password.val().trim()
                };

            if (_.isNull(data.username) || data.username == '') {
                error += this.strings["LOGIN_ERROR_USERNAME"] + "\n";
            }

            if (_.isNull(data.password) || data.password == '') {
                error += this.strings["LOGIN_ERROR_PASSWORD"] + "\n";
            }

            if (!_.isEmpty(error)) {
                Hybreed.UI.hideSpinner();

                if (Hybreed.mobile) {
                    navigator.notification.alert(error, null, " ");
                } else {
                    alert(error);
                }

            } else {
                this.trigger("login:click:login", data);
            }
        },

        sendFocusPassword: function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);

            if (code == 13) {
                this.ui.password.focus();

                if (Hybreed.mobile && Hybreed.platform == "android") {
                    cordova.plugins.Keyboard.show();
                }
            }
        },

        sendFocusButton: function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                this.ui.btnLogin.focus();
                this.launchLogin();
            }
        },

        showHidePassword: function () {
            var change = "";

            if (_.isEqual(this.ui.password.attr("type"), "password")) {
                change = "text";
                this.ui.showHidePassword.removeClass("iconShowPassword").addClass("iconHidePassword");
            } else {
                change = "password";
                this.ui.showHidePassword.removeClass("iconHidePassword").addClass("iconShowPassword");
            }

            this.ui.password.attr("type", change);
        }
    });

    return LoginView;

});
