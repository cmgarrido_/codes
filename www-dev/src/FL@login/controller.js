/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2015, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "broker", "FL@login/views/login", "FL@login/views/forgotPassword", "text!data/translations.json"],
    function (Hybreed, Broker, LoginView, ForgotPassword, Strings) {

        "use strict";

        var API,
            PrivateAPI;

        // VIEWS
        var loginView = null,
            forgotPasswordView = null,
            strings = JSON.parse(Strings);

        /**
         *     *************  PUBLIC API  ************
         */

        API = {

            showLoginView: function () {

                loginView = new LoginView({
                    strings: strings
                });

                PrivateAPI.associateEventsLogin();

                Broker.showViewInContent(loginView, "content");
            }

        };


        /**
         *     *************   PRIVATE API  ************
         */
        PrivateAPI = {


            showPasswordView: function () {
                forgotPasswordView = new ForgotPassword({
                    strings: strings
                });

                PrivateAPI.associateEventsForgotPassword();

                Broker.showViewInContent(forgotPasswordView, "content");
            },

            associateEventsLogin: function () {
                var that = this;

                loginView.on("login:click:login", function (dataUser) {
                    // TODO
                    Broker.commands.execute("FL@home:startHome");

                });

                loginView.on("login:click:back", function () {
                    Broker.commands.execute("FL@home:startHome");
                });

                loginView.on("login:click:forgotPassword", function () {
                    PrivateAPI.showPasswordView();
                });

            },

            associateEventsForgotPassword: function () {
                var that = this;

                forgotPasswordView.on("forgotPassword:click:back", function () {
                    API.showLoginView();
                });

                forgotPasswordView.on("forgotPassword:click:send", function () {
                    // TODO
                    API.showLoginView();
                });
            }
        };

        return API;

    });