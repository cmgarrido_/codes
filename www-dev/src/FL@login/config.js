/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2015, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(function (require) {
    "use strict";

    return {
        /* Define CSS to inject on Module load */
        css_login: require("css!src/FL@login/views/login.css"),
        css_forgotPassword: require("css!src/FL@login/views/forgotPassword.css"),

        /* Define HTMLs used in the views */
        html_login: require("text!src/FL@login/views/login.html"),
        html_forgotPassword: require("text!src/FL@login/views/forgotPassword.html")
    };
});