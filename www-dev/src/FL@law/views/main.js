/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "marionette", "FL@law/config"],
    function (Hybreed, Marionette, Config) {

        "use strict";


        var MainLawView = Marionette.ItemView.extend({

            template: _.template(Config.html_mainLaw),

            tagName: "div",

            ui: {
                "scrollTOC": "#scrollTOC",
                "mainAction": ".tocItem"
            },

            events: function () {
                var events = {},
                    event = Hybreed.UI.getEventByPlatform();

                events[event + " @ui.mainAction"] = "executeAction";

                return events;
            },

            className: "mainLaw",


            initialize: function (options) {
                this.collection = options.collection;
                this.model = options.model;
                this.strings = options.strings;
                this.scroll = null;
            },

            serializeData: function () {
                return {
                    law: this.model.toJSON(),
                    strings: this.strings,
                    toc: this.collection.toJSON()
                };
            },

            onShow: function () {

                this.scroll = Hybreed.UI.generateScroll(this.ui.scrollTOC[0], {
                    scrollbars: true,
                    bounce: true,
                    mouseWheel: true,
                    click: false,
                    tap: true,
                    preventDefaultException: {tagName: /^(input)$/}
                });

                Hybreed.UI.hideSpinner();
            },

            onDestroy: function () {
                if (!_.isNull(this.scroll)) {
                    this.scroll.destroy();
                    this.scroll = null;
                }
            },

            executeAction: function(ev){
                var id = $(ev.currentTarget).attr("data-id");

                this.trigger("law:click:mainAction", this.collection.get(id));
            }

        });

        return MainLawView;

    });