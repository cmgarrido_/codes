/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "marionette", "FL@law/config"], function (Hybreed, Marionette, Config) {

    "use strict";

    var LawTocView = Marionette.ItemView.extend({

        template: _.template(Config.html_tocLaw),

        tagName: "div",

        className: "tocLaw",

        ui: {
            back: ".iconBackBlack",
            scrollTOC: "#scrollTOC",
            mainAction: ".tocItem"
        },

        events: function () {
            var events = {},
                event = Hybreed.UI.getEventByPlatform();

            events[event + " @ui.mainAction"] = "executeAction";

            return events;
        },

        triggers: {
            "click @ui.back": "tocLaw:click:back"
        },

        initialize: function (options) {
            this.scroll = null;
            this.strings = options.strings;
            this.subTitle = options.subTitle;
        },

        serializeData: function () {
            return {
                strings: this.strings,
                model: this.model.toJSON(),
                subTitle: this.subTitle,
                items: this.collection.toJSON()
            };
        },

        onShow: function () {

            this.scroll = Hybreed.UI.generateScrollProbe(this.ui.scrollTOC[0], null);

            Hybreed.UI.hideSpinner();
        },

        onDestroy: function () {

            if (!_.isNull(this.scroll)) {
                this.scroll.destroy();
                this.scroll = null;
            }
        },

        executeAction: function(ev){
            var id = $(ev.currentTarget).attr("data-id");

            this.trigger("tocLaw:click:mainAction", this.collection.get(id));
        }

    });

    return LawTocView;

});
