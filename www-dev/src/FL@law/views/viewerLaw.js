/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "marionette", "FL@law/config"], function (Hybreed, Marionette, Config) {

    "use strict";

    var ViewerNewsView = Marionette.ItemView.extend({

        template: _.template(Config.html_viewerLaw),

        tagName: "div",

        className: "viewerLaw",

        ui: {
            back: ".iconBackBlack",
            contentViewerLaw: ".contentViewerLaw"
        },

        events: {},

        triggers: {
            "click @ui.back": "viewerLaw:click:back"
        },

        initialize: function (options) {
            this.scroll = null;
            this.modelLaw = options.modelLaw;
            this.strings = options.strings;
        },

        serializeData: function () {
            return {
                strings: this.strings,
                law: this.model.toJSON(),
                infoLaw: this.modelLaw.toJSON()
            };
        },

        onShow: function () {

            // CACHE
            this.$head = $("head");

            this.$head.find('link[href="assets/hybreed.css"]').remove();

            this.scroll = Hybreed.UI.generateScrollProbe(this.ui.contentViewerLaw[0], null);
            Hybreed.UI.hideSpinner();
        },

        onDestroy: function () {

            this.$head.append('<link rel="stylesheet" type="text/css" media="all" href="assets/hybreed.css"/>');

            if (!_.isNull(this.scroll)) {
                this.scroll.destroy();
                this.scroll = null;
            }
        }

        /**
         * -------------------------------------------------------------- ACTIONS
         **/


    });

    return ViewerNewsView;

});
