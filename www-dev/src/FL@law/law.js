/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
require.config({
    paths: {
        "FL@law": "src/FL@law"
        // @build (keep line)
    }
});

define(["broker", "FL@law/controller"], function (Broker, Controller) {

    "use strict";

    var API;

    Broker.commands.setHandler("FL@law:startLaw", function (model) {
        API.startLaw(model);
    });

    Broker.commands.setHandler("FL@law:startViewer", function (model) {
        API.startViewer(model);
    });

    API = {

        startLaw: function (model) {
            Controller.startLaw(model);
        },

        startViewer: function (model) {
            Controller.startReaderLaw(model);
        }
    };
});