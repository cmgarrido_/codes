/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "broker", "FL@law/views/main", "FL@law/views/viewerLaw", "FL@law/views/tocLaw", "text!data/lawTOC.json", "text!data/contentLaw.html", "text!data/translations.json"],
    function (Hybreed, Broker, MainLawView, ViewerLawView, TocLawView, LawTOCData, ContentLaw, Strings) {

        "use strict";

        var API,
            PrivateAPI;

        // VIEWS
        var layoutMenuLeftView = null,
            mainLawView = null,
            viewerLawView = null,
            tocLawView = null;

        // DATA
        var strings = JSON.parse(Strings),
            modelLaw = null,
            lawTOCCollection = null,
            contentLawModel = null;

        // OTHERS
        /*
         *  - command: para volver a un modulo exterior que invocó al visor de leyes
         *  - section: pestaña desde que se lanza el visor de leyes
         *  - data: información útil para volver, por ejemplo scrollTo
         * */
        var infoToBack = {},
            crumbLawTOC = [];

        /**
         *     *************  PUBLIC API  ************
         */

        API = {

            startLaw: function (model) {

                modelLaw = model;

                PrivateAPI.getLawTOC(model)
                    .then(function () {
                        PrivateAPI.showMainLawView();
                    }).fail(function () {
                        Hybreed.UI.hideSpinner();
                    });
            },

            startReaderLaw: function (lawModel, back) {

                PrivateAPI.getContentLaw();

                //infoToBack = back;

                modelLaw = lawModel;

                viewerLawView = new ViewerLawView({strings: strings, model: contentLawModel, modelLaw: modelLaw});

                PrivateAPI.associatedEventsViewerLaw();

                Broker.showViewInContent(viewerLawView, "content");
            }

        };


        /**
         *     *************   PRIVATE API  ************
         */
        PrivateAPI = {

            /***************   START DATA  *************/

            getContentLaw: function () {
                contentLawModel = new Backbone.Model({"content": ContentLaw}, {});
            },

            getLawTOC: function (lawModel) {
                var deferredValue = $.Deferred();

                if (_.isNull(lawTOCCollection)) {

                    var Model = Backbone.Model.extend({
                        parse: function (data) {
                            if (!_.isUndefined(data.levels)) {
                                data.levels = new Collection(data.levels, {parse: true});
                            }
                            return data;
                        }
                    });

                    var Collection = Backbone.Collection.extend({
                        model: Model
                    });

                    lawTOCCollection = new Collection(JSON.parse(LawTOCData), {parse: true});

                    deferredValue.resolve();
                } else {
                    deferredValue.resolve();
                }

                return deferredValue.promise();
            },


            /***************   SHOW VIEWS  *************/

            showLayoutMenuLeft: function () {
                if (_.isNull(layoutMenuLeftView) || layoutMenuLeftView.isDestroyed) {
                    layoutMenuLeftView = Broker.reqres.request("FL@menuLeft:generateMenuLeft");
                    Broker.showViewInContent(layoutMenuLeftView, "content");
                } else {
                    layoutMenuLeftView.slideOut();
                }
            },

            showMainLawView: function () {

                PrivateAPI.showLayoutMenuLeft();

                mainLawView = new MainLawView({model: modelLaw, collection: lawTOCCollection, strings: strings});

                PrivateAPI.associatedEventsMainLaw();

                Broker.showViewInLayout(mainLawView, layoutMenuLeftView.contentCenter);

                Hybreed.UI.hideSpinner();
            },

            showLawTOC: function () {

                PrivateAPI.getLawTOC()
                    .then(function () {

                        var collectionAux = lawTOCCollection,
                            subTitle = "";

                        _.each(crumbLawTOC, function (element) {
                            subTitle = collectionAux.get(element).get('title');
                            collectionAux = collectionAux.get(element).get('levels');
                        });

                        tocLawView = new TocLawView({
                            strings: strings,
                            model: modelLaw,
                            collection: collectionAux,
                            subTitle: subTitle
                        });

                        PrivateAPI.associatedEventsTocLawView();

                        Broker.showViewInContent(tocLawView, "content");

                        Hybreed.UI.hideSpinner();
                    }).fail(function () {
                        Hybreed.UI.hideSpinner();
                    });

            },

            /***************   EVENTS  *************/

            associatedEventsMainLaw: function (model) {

                mainLawView.on("law:click:mainAction", function (model) {
                    if (model.has('levels')) {
                        crumbLawTOC.push(model.id);
                        PrivateAPI.showLawTOC();
                    } else {
                       API.startReaderLaw(modelLaw);
                    }
                });

            },

            associatedEventsViewerLaw: function () {
                viewerLawView.on("viewerLaw:click:back", function () {
                    Broker.commands.execute("FL@home:startHome");
                });
            },


            associatedEventsTocLawView: function () {

                tocLawView.on("tocLaw:click:mainAction", function (model) {
                    if (model.has('levels')) {
                        crumbLawTOC.push(model.id);
                        PrivateAPI.showLawTOC();
                    } else {
                        API.startReaderLaw(modelLaw);
                    }
                });

                tocLawView.on("tocLaw:click:back", function () {
                    crumbLawTOC.pop();
                    if (crumbLawTOC.length > 0) {
                        PrivateAPI.showLawTOC();
                    } else {
                        PrivateAPI.showMainLawView();
                    }
                });
            }
        };

        return API;

    });