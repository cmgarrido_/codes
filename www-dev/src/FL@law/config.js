/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2016, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
define(function (require) {

    "use strict";

    return {
        /* Define CSS to inject on Module load */
        css_viewerLaw: require("css!src/FL@law/views/viewerLaw.css"),
        css_mainLaw: require("css!src/FL@law/views/main.css"),
        css_tocLaw: require("css!src/FL@law/views/tocLaw.css"),

        /* Define HTMLs used in the views */
        html_viewerLaw: require("text!src/FL@law/views/viewerLaw.html"),
        html_mainLaw: require("text!src/FL@law/views/main.html"),
        html_tocLaw: require("text!src/FL@law/views/tocLaw.html")

    };
});
