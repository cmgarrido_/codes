/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2016, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
define(["marionette", "hybreed", "FL_GV@genericsViews/config"], function (Marionette, Hybreed, Config) {

    "use strict";

    var LayoutHome = Marionette.LayoutView.extend({

        template: _.template(Config.html_layoutTabs),

        tagName: "div",

        className: function () {
            return "layoutTabs" + " " + this.options.textClass;
        },

        regions: {
            contentTab: ".contentTab"
        },

        ui: {
            optMenuLeft: "#optMenuLeft",
            tabs: ".tabs",
            tab: ".cellTab"
        },

        events: {
            "click @ui.tab": "changeTab"
        },

        triggers: {
            "click @ui.optMenuLeft": "menuLeft:slideOut"
        },

        initialize: function (options) {
            this.strings = options.strings;
            this.title = options.title;
            this.tabs = options.tabs;
            this.optRight = options.optRight;
            this.textClass = options.textClass;
        },

        serializeData: function () {
            return {
                title: this.title,
                optRight: this.optRight,
                tabs: this.tabs,
                strings: this.strings
            };
        },

        changeTab: function (ev) {
            var $element = $(ev.currentTarget);

            if (!$element.hasClass("selected")) {
                this.ui.tab.removeClass("selected");
                $element.addClass("selected");
                this.trigger("layoutTab:click:" + $element.attr('data-id'));
            }

        },

        selectTab: function (id) {
            this.ui.tab.removeClass("selected");
            this.ui.tabs.find('[data-id="' + id + '"]').addClass("selected");
        }
    });

    return LayoutHome;
});