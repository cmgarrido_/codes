/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2016, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
define(function (require) {

    "use strict";

    return {
        /* Define CSS to inject on Module load */
        css_layoutTabs: require("css!src/FL_GV@genericsViews/layoutTabs/layoutTabs.css"),
        css_listInfinite: require("css!src/FL_GV@genericsViews/listInfinite/listInfinite.css"),
        css_itemNew: require("css!src/FL_GV@genericsViews/listInfinite/listTemplates/itemNew.css"),

        /* Define HTMLs used in the views */
        html_layoutTabs: require("text!src/FL_GV@genericsViews/layoutTabs/layoutTabs.html"),
        html_listInfinite: require("text!src/FL_GV@genericsViews/listInfinite/listInfinite.html"),
        html_itemNew: require("text!src/FL_GV@genericsViews/listInfinite/listTemplates/itemNew.html")

    };
});
