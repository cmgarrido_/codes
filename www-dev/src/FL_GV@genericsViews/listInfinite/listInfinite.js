define(["marionette", "hybreed", "FL_GV@genericsViews/config"], function (Marionette, Hybreed, Config) {

    "use strict";

    var ListNews = Marionette.ItemView.extend({

        template: _.template(Config.html_listInfinite),

        tagName: "div",

        className: function () {
            return "listInfinite" + (this.options.typeListTemplate ? " " + this.options.typeListTemplate : "");
        },

        ui: {
            "wrapperListInfinite": ".wrapper_listInfinite",
            "scrollbar": ".scrollbar",
            "wrapperScrollbar": ".wrapperScrollbar"
        },

        initialize: function (options) {
            this.collection = options.collection;
            this.arrayItems = this.collection.toJSON();
            this.typeListTemplate = options.typeListTemplate;
            this.scrollTo = !_.isUndefined(options.scrollTo) ? options.scrollTo : null;

            this.scroll = null;
            this.scrollBar = null;
        },

        onShow: function () {

            this.generateScroll();
            this.generateScrollbar();

            if (!_.isNull(this.scrollTo)) {
                this.scrollToElement();
            }

            Hybreed.UI.hideSpinner();
        },

        onDestroy: function () {
            if (!_.isNull(this.scroll)) {
                this.scroll.destroy();
            }

            if (!_.isNull(this.scrollBar)) {
                this.scrollBar.destroy();
            }
        },

        generateScroll: function () {
            var that = this;

            if (this.arrayItems.length > 0) {

                var dataset = function (start, count) {
                    var data = that.arrayItems.slice(start, start + count);
                    this.updateCache(start, data);
                };


                var dataFiller = function (el, data) {

                    if (!_.isUndefined(data) && !_.isEmpty(data)) {
                        //  $(el).attr("data-id", data["ID"]);


                        el.innerHTML = _.template(Config["html_" + that.typeListTemplate])(data);

                        $(el).show(0, function () {
                            /*setTimeout(function () {
                             $(thatEl.getElementsByClassName("description")[0]).dotdotdot({
                             watch: "window"
                             });
                             }, 90);*/
                        });

                    } else {
                        $(el).hide();
                    }
                };

                this.scroll = Hybreed.UI.generateScrollInfinite('.scroller', null, ".scroller .listItemInfinite", this.arrayItems.length, dataset, dataFiller);

                this.associateEventsItem("tap");
            }

        },

        generateScrollbar: function () {
            var that = this,
                options = {
                    scrollbars: true,
                    interactiveScrollbars: true,
                    shrinkScrollbars: 'scale',
                    fadeScrollbars: false,
                    bounce: true,
                    mouseWheel: true,
                    probeType: 3,
                    click: false,
                    tap: false,
                    preventDefaultException: {tagName: /^(input)$/}
                };

            if (!_.isNull(this.scroll) && (Math.abs(that.scroll.maxScrollY)) > 0) {
                this.ui.wrapperScrollbar.height(Math.abs(that.scroll.maxScrollY) + that.scroll.wrapperHeight);
                this.scrollBar = Hybreed.UI.generateScrollProbe(this.ui.scrollbar[0], options, true);

                setTimeout(function () {
                    if (!_.isNull(that.scrollBar)) {
                        that.scrollBar.refresh();
                    }
                }, 300);

                this.associatedEventsScrollbar();
            }
        },

        associatedEventsScrollbar: function () {

            var that = this;

            /** SCROLL BAR **/
            if (!_.isNull(that.scrollBar)) {

                that.scrollBar.on('scroll', function () {
                    var scrollY = this.y;

                    if (scrollY < 0 && scrollY >= that.scroll.maxScrollY) {
                        that.scroll.scrollTo(that.scroll.x, scrollY);
                        that.scroll.refresh();
                    } else if (scrollY < that.scroll.y && that.scroll.y > that.scroll.maxScrollY) {
                        that.scroll.scrollTo(that.scroll.x, that.scroll.maxScrollY);
                        that.scroll.refresh();
                    }
                });
            }

            /** SCROLL CONTENT **/
            if (!_.isNull(that.scroll)) {

                that.scroll.on('scroll', function () {
                    that.scrollBar.scrollTo(that.scrollBar.x, (this.y >= this.maxScrollY ? this.y : that.scrollBar.maxScrollY));
                });
            }
        },

        associateEventsItem: function (event) {

            var that = this;

            this.ui.wrapperListInfinite.delegate(".rightElement", event, function () {
                var element = $(this).parents(".newElement"),
                    id = element.attr("data-id"),
                    dataElement = _.findWhere(that.arrayItems, {"id": parseInt(id)});

                dataElement["bookmark"] = !dataElement["bookmark"];

                element.attr("data-bookmark", dataElement["bookmark"]);

                that.trigger("listInfinite:tap:bookmark", that.collection.get(id));
            });

            this.ui.wrapperListInfinite.delegate(".viewElement", event, function () {
                var element = $(this).parents(".newElement"),
                    id = element.attr("data-id");

                that.trigger("listInfinite:tap:item", that.collection.get(id));
            });
        },

        scrollToElement: function () {
            var that = this,
                index = _.findIndex(this.arrayItems, function (item) {
                    return _.isEqual(that.scrollTo, item.id);
                });

            if (index > 0) {
                this.scroll.scrollTo(0, -(this.ui.wrapperListInfinite.find('li').outerHeight() * index), 0);
                this.scroll.refresh();

                if (!_.isNull(this.scrollBar)) {
                    this.scrollBar.scrollTo(0, -(this.ui.wrapperListInfinite.find('li').outerHeight() * index), 0);
                    this.scrollBar.refresh();
                }
            }
        }

    });

    return ListNews;
});