/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2016, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
define(["broker", "hybreed", "hybreed-ui"],
    function (Broker, Hybreed) {

        "use strict";

        var API,
            PrivateAPI;

        /**
         *     *************  PUBLIC API  ************
         */

        API = {

            startModule: function () {

                Hybreed.UI.createSpinner(document.getElementById('spinner'));
                Hybreed.UI.showSpinner();

                PrivateAPI.startHome();

            }

        };

        /**
         *     *************   PRIVATE API  ************
         */
        PrivateAPI = {

            startHome: function() {
                Broker.commands.execute("FL@home:startHome");
            }

        };


        return API;
    });