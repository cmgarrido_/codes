/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2016, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
require.config({
    paths: {
        "FL@main": "src/FL@main",
        "FL_GV@genericsViews": "src/FL_GV@genericsViews"
        // @build (keep line)
    }
});

define(["broker", "FL@main/controller"], function (Broker, Controller) {

    "use strict";

    var API;

    Broker.commands.setHandler("FL@main:startModule", function () {
        API.start();
    });

    API = {

        start: function () {
            return Controller.startModule();
        }
    };
});

