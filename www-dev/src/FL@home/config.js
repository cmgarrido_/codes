/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2016, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
define(function (require) {

    "use strict";

    return {
        /* Define CSS to inject on Module load */
        css_layoutHome: require("css!src/FL@home/views/layoutHome.css"),
        css_downloads: require("css!src/FL@home/views/downloads.css"),
        css_laws: require("css!src/FL@home/views/laws.css"),

        /* Define HTMLs used in the views */
        html_layoutHome: require("text!src/FL@home/views/layoutHome.html"),
        html_downloads: require("text!src/FL@home/views/downloads.html"),
        html_downloadsItem: require("text!src/FL@home/views/downloadsItem.html"),
        html_laws: require("text!src/FL@home/views/laws.html"),
        html_lawsItem: require("text!src/FL@home/views/lawsItem.html")
    };
});
