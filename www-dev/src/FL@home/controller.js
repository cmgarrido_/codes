/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2015, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "broker", "FL@home/views/layoutHome", "FL@home/views/downloads", "FL@home/views/laws", "text!data/translations.json", "text!data/mainLaws.json", "text!data/basicLegislation.json"],
    function (Hybreed, Broker, LayoutHomeView, DownloadsView, LawsView, Strings, MainLaws, BasicLegislation) {

        "use strict";

        var API,
            PrivateAPI;

        // VIEWS
        var layoutMenuLeftView = null,
            layoutHomeView = null,
            downloadsView = null,
            mainLawsView = null,
            favoritesView = null,
            basicLegislationView = null,
            strings = JSON.parse(Strings);

        // COLLECTIONS
        var mainLawsCollection = null,
            favoritesCollection = null,
            basicLegislationCollection = null,
            downloadsCollection = new Backbone.Collection([
                {
                    id: "ET-05/2016",
                    title: "Estatuto de los trabajadores",
                    date: "15.05.2016"
                },
                {
                    id: "CV-11/2015",
                    title: "Codigo Civil",
                    date: "27.11.2015"
                },
                {
                    id: "CV-08/2015",
                    title: "Codigo Civil",
                    date: "20.08.2015"
                }
            ], {});

        //
        var crumbBasicLegislation = [];


        /**
         *     *************  PUBLIC API  ************
         */

        API = {

            startHome: function (tab) {

                PrivateAPI.showLayoutMenuLeft();

                layoutHomeView = new LayoutHomeView({strings: strings});

                PrivateAPI.associatedEventsLayoutHome();

                Broker.showViewInLayout(layoutHomeView, layoutMenuLeftView.contentCenter);

                if(_.isUndefined(tab) || _.isNull(tab)) {
                    PrivateAPI.showMainLaws();
                } else {
                    crumbBasicLegislation = [];
                    layoutHomeView.selectTabBasicLegislation();
                    PrivateAPI.showBasicLegislation();
                }
            },

            startDownloads: function () {

                PrivateAPI.showLayoutMenuLeft();

                downloadsView = new DownloadsView({collection: downloadsCollection, strings: strings});

                PrivateAPI.associatedEventsDownloads();

                Broker.showViewInLayout(downloadsView, layoutMenuLeftView.contentCenter);
            }

        };


        /**
         *     *************   PRIVATE API  ************
         */
        PrivateAPI = {


            /***************   START DATA  *************/

            startMainLawsCollection: function () {
                mainLawsCollection = new Backbone.Collection(JSON.parse(MainLaws), {});
            },

            startBasicLegislationCollection: function () {

                var Model = Backbone.Model.extend({

                    parse: function (data) {
                        if (!_.isUndefined(data.levels)) {
                            data.levels = new Collection(data.levels, {parse: true});
                        }

                        return data;
                    }
                });

                var Collection = Backbone.Collection.extend({
                    model: Model
                });

                basicLegislationCollection = new Collection(JSON.parse(BasicLegislation), {parse: true});
            },

            /***************   SHOW VIEWS  *************/

            showLayoutMenuLeft: function () {
                if (_.isNull(layoutMenuLeftView) || layoutMenuLeftView.isDestroyed) {
                    layoutMenuLeftView = Broker.reqres.request("FL@menuLeft:generateMenuLeft");
                    Broker.showViewInContent(layoutMenuLeftView, "content");
                } else {
                    layoutMenuLeftView.slideOut();
                }
            },

            showMainLaws: function () {
                if (_.isNull(mainLawsCollection)) {
                    PrivateAPI.startMainLawsCollection();
                }

                mainLawsView = new LawsView({
                    strings: strings,
                    collection: mainLawsCollection,
                    optRight: false,
                    showBack: false
                });

                PrivateAPI.associatedEventsMainLaws();

                Broker.showViewInLayout(mainLawsView, layoutHomeView.contentHome);
            },

            showFavorites: function () {

                favoritesCollection = new Backbone.Collection(mainLawsCollection.where({'favorite': true}), {});

                favoritesView = new LawsView({
                    strings: strings,
                    collection: favoritesCollection,
                    optRight: false,
                    showBack: false
                });

                PrivateAPI.associatedEventsFavorites();

                Broker.showViewInLayout(favoritesView, layoutHomeView.contentHome);
            },

            showBasicLegislation: function (animation) {
                if (_.isNull(basicLegislationCollection)) {
                    PrivateAPI.startBasicLegislationCollection();
                }

                var collectionAux = basicLegislationCollection;

                _.each(crumbBasicLegislation, function (element) {
                    collectionAux = collectionAux.get(element).get('levels');
                });

                basicLegislationView = new LawsView({
                    strings: strings,
                    collection: collectionAux,
                    optRight: true,
                    showBack: (crumbBasicLegislation.length > 0)
                });

                PrivateAPI.associatedEventsBasicLegislation();

                if (_.isUndefined(animation) || _.isNull(animation)) {
                    Broker.showViewInLayout(basicLegislationView, layoutHomeView.contentHome);
                } else {
                    Broker.showViewInLayoutRegionAnimation(basicLegislationView, layoutHomeView.contentHome, animation);
                }
            },

            /***************   EVENTS  *************/

            associatedEventsLayoutHome: function () {

                layoutHomeView.on("home:click:news", function () {
                    Broker.commands.execute("FL@news:startNews");
                });

                layoutHomeView.on("layoutHome:click:mainLaws", function () {
                    PrivateAPI.showMainLaws();
                });

                layoutHomeView.on("layoutHome:click:basicLaw", function () {
                    PrivateAPI.showBasicLegislation();
                });

                layoutHomeView.on("layoutHome:click:favoritesLaws", function () {
                    PrivateAPI.showFavorites();
                });
            },

            associatedEventsDownloads: function () {

                downloadsView.on("childview:download:click:delete", function (view) {
                    downloadsCollection.remove(view.model);
                });

                downloadsView.on("childview:download:click:view", function (view) {
                    Broker.commands.execute("FL@law:startLaw", view.model);
                });
            },

            associatedEventsMainLaws: function () {

                mainLawsView.on("childview:law:click:leftAction", function (view) {
                    view.model.set('favorite', !view.model.get('favorite'));
                });

                mainLawsView.on("childview:law:click:mainAction", function (view) {
                    Broker.commands.execute("FL@law:startLaw", view.model);
                });
            },

            associatedEventsFavorites: function () {

                favoritesView.on("childview:law:click:leftAction", function (view) {
                    mainLawsCollection.get(view.model.id).set('favorite', false);
                    favoritesCollection.remove(view.model);
                });

                favoritesView.on("childview:law:click:mainAction", function (view) {
                    Broker.commands.execute("FL@law:startLaw", view.model);
                });
            },

            associatedEventsBasicLegislation: function () {

                basicLegislationView.on("childview:law:click:leftAction", function (view) {
                    view.model.set('favorite', !view.model.get('favorite'));
                });

                basicLegislationView.on("childview:law:click:mainAction", function (view) {
                    if (view.model.has('levels')) {
                        crumbBasicLegislation.push(view.model.id);
                        PrivateAPI.showBasicLegislation("slideInRight");
                    } else {
                        Broker.commands.execute("FL@law:startLaw", new Backbone.Model({title: "Basic Legislation", id: 1}));
                    }
                });

                basicLegislationView.on("law:click:back", function () {
                    crumbBasicLegislation.pop();
                    PrivateAPI.showBasicLegislation("slideInLeft");
                });
            }

        };

        return API;

    });