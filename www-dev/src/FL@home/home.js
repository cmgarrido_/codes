/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
require.config({
    paths: {
        "FL@home": "src/FL@home"
        // @build (keep line)
    }
});

define(["broker", "FL@home/controller"], function (Broker, Controller) {

    "use strict";

    var API;

    Broker.commands.setHandler("FL@home:startHome", function (tab) {
        API.startHome(tab);
    });

    Broker.commands.setHandler("FL@home:startDownloads", function () {
        API.startDownloads();
    });

    API = {

        startHome: function (tab) {
            Controller.startHome(tab);
        },

        startDownloads: function () {
            Controller.startDownloads();
        }
    };
});