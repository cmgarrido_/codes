/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "marionette", "FL@home/config"],
    function (Hybreed, Marionette, Config) {

        "use strict";


        var DownloadEmptyView = Marionette.ItemView.extend({

            className: "downloadEmpty",

            tagName: "li",

            template: _.template('<%= stringText %>'),

            initialize: function (options) {
                this.strings = options.strings;
            },

            serializeData: function () {
                return {
                    stringText: this.strings["DOWNLOADS_NO_RESULTS"]
                }
            }
        });

        var DownloadItemView = Marionette.ItemView.extend({

            template: _.template(Config.html_downloadsItem),

            tagName: "li",

            className: "downloadItem",

            ui: {
                "delete": ".deleteDownload",
                "view": ".viewDownload"
            },

            triggers: function () {
                var triggers = {},
                    event = Hybreed.UI.getEventByPlatform();

                triggers[event + " @ui.delete"] = "download:click:delete";
                triggers[event + " @ui.view"] = "download:click:view";

                return triggers;
            }
        });


        var DownloadView = Marionette.CompositeView.extend({

            template: _.template(Config.html_downloads),

            tagName: "div",

            ui: {
                "scrollDownloads": "#scrollDownloads"
            },

            childView: DownloadItemView,

            childViewContainer: ".listDownloads",

            className: "downloads",

            emptyView: DownloadEmptyView,

            emptyViewOptions: function () {
                return {
                    strings: this.strings
                }
            },

            initialize: function (options) {
                this.collection = options.collection;
                this.strings = options.strings;
                this.scroll = null;
            },

            serializeData: function () {
                return {
                    strings: this.strings
                };
            },

            onShow: function () {

                this.scroll = Hybreed.UI.generateScroll(this.ui.scrollDownloads[0], {
                    scrollbars: true,
                    bounce: true,
                    mouseWheel: true,
                    click: false,
                    tap: true,
                    preventDefaultException: {tagName: /^(input)$/}
                });

                Hybreed.UI.hideSpinner();
            },

            onDestroy: function () {
                if (!_.isNull(this.scroll)) {
                    this.scroll.destroy();
                    this.scroll = null;
                }
            }
        });

        return DownloadView;

    });