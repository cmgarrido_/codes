/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "marionette", "FL@home/config"],
    function (Hybreed, Marionette, Config) {

        "use strict";


        var LawsEmptyView = Marionette.ItemView.extend({

            className: "lawsEmpty",

            tagName: "li",

            template: _.template('<%= stringText %>'),

            initialize: function (options) {
                this.strings = options.strings;
            },

            serializeData: function () {
                return {
                    stringText: this.strings["HOME_NO_RESULTS"]
                }
            }
        });

        var LawItemView = Marionette.ItemView.extend({

            template: _.template(Config.html_lawsItem),

            tagName: "li",

            className: "lawItem",

            ui: {
                "leftAction": ".leftAction",
                "mainAction": ".mainAction",
                "favorite": ".favorite"
            },

            modelEvents: {
                "change:favorite": "changeFavorite"
            },

            attributes: function () {
                return {
                    "data-favorite": this.model.get('favorite')
                }
            },

            triggers: function () {
                var triggers = {},
                    event = Hybreed.UI.getEventByPlatform();

                triggers[event + " @ui.leftAction"] = "law:click:leftAction";
                triggers[event + " @ui.mainAction"] = "law:click:mainAction";

                return triggers;
            },

            initialize: function (options) {
                this.optRight = options.optRight;

                if (this.optRight) {
                    this.optRight = this.model.has('levels');
                }
            },

            serializeData: function () {
                return {
                    item: this.model.toJSON(),
                    optRight: this.optRight
                }
            },

            changeFavorite: function () {
                this.$el.attr("data-favorite", this.model.get('favorite'));
            }
        });


        var LawsView = Marionette.CompositeView.extend({

            template: _.template(Config.html_laws),

            tagName: "div",

            className: "laws",

            ui: {
                "scrollLaws": "#scrollLaws",
                "back": "#back"
            },

            childView: LawItemView,

            childViewContainer: ".listLaws",

            childViewOptions: function () {
                return {
                    optRight: this.optRight
                }
            },

            emptyView: LawsEmptyView,

            emptyViewOptions: function () {
                return {
                    strings: this.strings
                }
            },

            triggers:  {
               "click @ui.back": "law:click:back"
            },

            initialize: function (options) {
                this.collection = options.collection;
                this.strings = options.strings;
                this.optRight = options.optRight;
                this.showBack = options.showBack;
                this.scroll = null;
            },

            serializeData: function () {
                return {
                    showBack: this.showBack
                };
            },

            onShow: function () {

                this.scroll = Hybreed.UI.generateScroll(this.ui.scrollLaws[0], {
                    scrollbars: true,
                    bounce: true,
                    mouseWheel: true,
                    click: false,
                    tap: true,
                    preventDefaultException: {tagName: /^(input)$/}
                });

                Hybreed.UI.hideSpinner();
            },

            onDestroy: function () {
                if (!_.isNull(this.scroll)) {
                    this.scroll.destroy();
                    this.scroll = null;
                }
            },

            onRemoveChild: function() {
                if (!_.isNull(this.scroll)) {
                    this.scroll.refresh();
                }
            }
        });

        return LawsView;

    });