/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2016, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
define(["marionette", "hybreed", "FL@home/config", "region-animation"],
    function (Marionette, Hybreed, Config, RegionAnimation) {

        "use strict";

        var LayoutHome = Marionette.LayoutView.extend({

            template: _.template(Config.html_layoutHome),

            tagName: "div",

            className: "layoutHome",

            regions: {
                contentHome:  {el: ".contentHome:last", regionClass: RegionAnimation}
            },

            ui: {
                optMenuLeft: "#optMenuLeft",
                tab: ".cellTab",
                contentTabBar: ".contentTabBar",
                tabBasicLaw: "#tabBasicLaw"
            },

            events: {
                "click @ui.tab": "changeTab"
            },

            triggers: {
                "click @ui.optMenuLeft": "menuLeft:slideOut",
                "click @ui.contentTabBar": "home:click:news"
            },

            initialize: function (options) {
                this.strings = options.strings;
            },

            serializeData: function () {
                return {
                    strings: this.strings
                };
            },

            changeTab: function (ev) {

                var $element = $(ev.currentTarget);

                if (!$element.hasClass("selected")) {

                    this.ui.tab.removeClass("selected");
                    $element.addClass("selected");

                    switch ($element.attr("id")) {
                        case "tabMainLaw":
                            this.trigger("layoutHome:click:mainLaws");
                            break;
                        case "tabBasicLaw":
                            this.trigger("layoutHome:click:basicLaw");
                            break;
                        case "tabFavorites":
                            this.trigger("layoutHome:click:favoritesLaws");
                            break;
                    }
                }
            },

            selectTabBasicLegislation: function() {
                this.ui.tab.removeClass("selected");
                this.ui.tabBasicLaw.addClass("selected");
            }
        });

        return LayoutHome;
    });