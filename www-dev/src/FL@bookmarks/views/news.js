/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "marionette", "FL_GV@genericsViews/config", "FL@bookmarks/config"],
    function (Hybreed, Marionette, ConfigGenericView) {

        "use strict";


        var NewEmptyView = Marionette.ItemView.extend({

            className: "bookmarkNewEmpty",

            tagName: "li",

            template: _.template('<%= stringText %>'),

            initialize: function (options) {
                this.strings = options.strings;
            },

            serializeData: function () {
                return {
                    stringText: this.strings["BOOKMARKS_TAB_NEWS_NO_RESULTS"]
                }
            }
        });

        var NewItemView = Marionette.ItemView.extend({

            template: _.template(ConfigGenericView.html_itemNew),

            tagName: "li",

            className: "itemNew",

            ui: {
                "delete": ".rightElement",
                "view": ".viewElement"
            },

            attributes: function () {
                return {
                    "data-id": this.model.id
                }
            },

            triggers: function () {
                var triggers = {},
                    event = Hybreed.UI.getEventByPlatform();

                triggers[event + " @ui.delete"] = "news:click:deleteBookmark";
                triggers[event + " @ui.view"] = "news:click:view";

                return triggers;
            }

        });


        var NewsView = Marionette.CollectionView.extend({

            tagName: "ul",

            childView: NewItemView,

            className: "bookmarkListNews",

            emptyView: NewEmptyView,

            emptyViewOptions: function () {
                return {
                    strings: this.strings
                }
            },

            initialize: function (options) {
                this.collection = options.collection;
                this.strings = options.strings;
                this.scrollTo = options.scrollTo;
                this.scroll = null;
            },

            serializeData: function () {
                return {
                    strings: this.strings
                };
            },

            onShow: function () {

                this.$container = $(".contentTab");

                this.scroll = Hybreed.UI.generateScroll(this.$container[0], {
                    scrollbars: true,
                    bounce: true,
                    mouseWheel: true,
                    click: true,
                    tap: true,
                    preventDefaultException: {tagName: /^(input)$/}
                });

                if (!_.isNull(this.scrollTo)) {
                    this.scrollToElement();
                }

                Hybreed.UI.hideSpinner();
            },

            onDestroy: function () {
                if (!_.isNull(this.scroll)) {
                    this.scroll.destroy();
                    this.scroll = null;
                }
            },

            onRemoveChild: function () {
                this.refreshScroll();
            },

            refreshScroll: function () {
                if (!_.isNull(this.scroll)) {
                    this.scroll.refresh();
                }
            },

            scrollToElement: function () {
                var $elements = this.$el.find('li[data-id="' + this.scrollTo + '"]');

                if ($elements.length > 0) {
                    if (_.isNull(this.scroll)) {
                        this.$container.scrollTo($elements[0], 0);
                    } else {
                        this.scroll.scrollToElement($elements[0], 0);
                    }
                }
            }
        });

        return NewsView;

    });