/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "marionette", "FL@bookmarks/config"],
    function (Hybreed, Marionette, Config) {

        "use strict";


        var LawEmptyView = Marionette.ItemView.extend({

            className: "bookmarkLawEmpty",

            tagName: "li",

            template: _.template('<%= stringText %>'),

            initialize: function (options) {
                this.strings = options.strings;
            },

            serializeData: function () {
                return {
                    stringText: this.strings["BOOKMARKS_TAB_BL_NO_RESULTS"]
                }
            }
        });

        var LawItemView = Marionette.ItemView.extend({

            template: _.template(Config.html_lawsBookmark),

            tagName: "li",

            className: "itemLaw",

            attributes: function () {
                return {
                    "data-bookmark": this.model.get('bookmark')
                }
            },

            events: function () {
                var events = {},
                    event = Hybreed.UI.getEventByPlatform();

                events[event] = "executeAction";

                return events;
            },

            executeAction: function (ev) {
                var $element = $(ev.target);
                if ($element.parent(".cellRight").length > 0 || $element.hasClass("cellRight")) {
                    this.trigger("laws:click:deleteBookmark");
                } else {
                    this.trigger("laws:click:view");
                }
            }

        });


        var NewsView = Marionette.CollectionView.extend({

            tagName: "ul",

            childView: LawItemView,

            className: "bookmarkListLaws",

            emptyView: LawEmptyView,

            childEvents: {
                "laws:click:deleteBookmark": function (view) {
                    this.deleteBookmark(view.model);
                }
            },

            emptyViewOptions: function () {
                return {
                    strings: this.strings
                }
            },

            initialize: function (options) {
                this.collection = options.collection;
                this.strings = options.strings;
                this.scroll = null;
            },

            serializeData: function () {
                return {
                    strings: this.strings
                };
            },

            onShow: function () {

                this.$container = $(".contentTab");

                this.scroll = Hybreed.UI.generateScroll(this.$container[0], {
                    scrollbars: true,
                    bounce: true,
                    mouseWheel: true,
                    click: false,
                    tap: true,
                    preventDefaultException: {tagName: /^(input)$/}
                });

                Hybreed.UI.hideSpinner();
            },

            onDestroy: function () {
                if (!_.isNull(this.scroll)) {
                    this.scroll.destroy();
                    this.scroll = null;
                }
            },

            refreshScroll: function () {
                if (!_.isNull(this.scroll)) {
                    this.scroll.refresh();
                }
            },

            deleteBookmark: function (model) {
                model.set("bookmark", false);
                this.collection.remove(model.id);
                this.refreshScroll();
            }

        });

        return NewsView;

    });