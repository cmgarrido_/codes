/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "broker", "FL_GV@genericsViews/layoutTabs/layoutTabs", "FL@bookmarks/views/news", "FL@bookmarks/views/laws", "text!data/news.json", "text!data/basicLegislationBookmarks.json", "text!data/translations.json"],
    function (Hybreed, Broker, LayoutBookmarks, ListNewsView, ListLawsView, DataNews, DataLaws, Strings) {

        "use strict";

        var API,
            PrivateAPI;

        // VIEWS
        var layoutMenuLeftView = null,
            layoutBookmarks = null,
            listNewsView = null,
            listLawsView = null;

        // DATA
        var strings = JSON.parse(Strings),
            collectionNews = null,
            collectionLaws = null;

        // HARD-CODE
        var newsArray = JSON.parse(DataNews),
            lawsArray = JSON.parse(DataLaws);

        // CONSTANTS
        var MODULE_BOOKMARKS = "BOOKMARKS";

        /**
         *     *************  PUBLIC API  ************
         */

        API = {

            startBookmarks: function (data) {

                layoutMenuLeftView = Broker.reqres.request("FL@menuLeft:generateMenuLeft");

                Broker.showViewInContent(layoutMenuLeftView, "content");

                layoutBookmarks = new LayoutBookmarks({
                    title: strings["BOOKMARKS_TITLE"],
                    tabs: [{
                        id: "tabNews",
                        title: strings["BOOKMARKS_TAB_NEWS"]
                    }, {
                        id: "tabsBL",
                        title: strings["BOOKMARKS_TAB_BL"]
                    }],
                    strings: strings,
                    optRight: false,
                    textClass: ""
                });

                PrivateAPI.associatedEventsLayoutBookmarks();

                Broker.showViewInLayout(layoutBookmarks, layoutMenuLeftView.contentCenter);

                PrivateAPI.showListNews(data);

                Hybreed.UI.hideSpinner();
            }

        };


        /**
         *     *************   PRIVATE API  ************
         */
        PrivateAPI = {

            /***************   START DATA  *************/

            getListNews: function () {
                var deferredValue = $.Deferred();

                if (_.isNull(collectionNews)) {
                    for (var i = 0; i < newsArray.length; i++) {
                        newsArray[i].bookmark = true;
                    }
                    collectionNews = new Backbone.Collection(newsArray);

                    deferredValue.resolve();
                } else {
                    deferredValue.resolve();
                }

                return deferredValue.promise();
            },

            getListLaws: function () {
                var deferredValue = $.Deferred();

                if (_.isNull(collectionLaws)) {
                    collectionLaws = new Backbone.Collection(lawsArray);
                    deferredValue.resolve();
                } else {
                    deferredValue.resolve();
                }

                return deferredValue.promise();
            },


            /***************   SHOW VIEWS  *************/

            showListNews: function (data) {
                var that = this;

                this.getListNews()
                    .then(function () {

                        listNewsView = new ListNewsView({
                            collection: collectionNews,
                            strings: strings,
                            scrollTo: (!_.isUndefined(data) && !_.isUndefined(data.scrollTo)) ? data.scrollTo : null
                        });

                        Broker.showViewInLayout(listNewsView, layoutBookmarks.contentTab);

                        that.associatedEventsListNewsView();
                    });

            },

            showListLaws: function () {
                var that = this;

                this.getListLaws()
                    .then(function () {

                        listLawsView = new ListLawsView({
                            collection: collectionLaws,
                            strings: strings
                        });

                        Broker.showViewInLayout(listLawsView, layoutBookmarks.contentTab);

                        that.associatedEventsListLawsView();
                    });

            },


            /***************   EVENTS  *************/

            associatedEventsLayoutBookmarks: function () {
                var that = this;

                layoutBookmarks.on("layoutTab:click:tabNews", function () {
                    that.showListNews();
                });

                layoutBookmarks.on("layoutTab:click:tabsBL", function () {
                    that.showListLaws();
                });
            },

            associatedEventsListNewsView: function () {

                listNewsView.on("childview:news:click:deleteBookmark", function (view) {
                    view.model.set("bookmark", false);
                    collectionNews.remove(view.model.id);
                });

                listNewsView.on("childview:news:click:view", function (view) {
                    Broker.commands.execute("FL@news:star:viewerNews", {
                        model: view.model,
                        back: {
                            command: "FL@bookmarks:startBookmarks",
                            data: {scrollTo: view.model.id}
                        }
                    });
                });
            },

            associatedEventsListLawsView: function () {

                listLawsView.on("childview:laws:click:view", function (view) {
                    Broker.commands.execute("FL@law:startLaw", view.model);
                });
            }

        };

        return API;

    });