/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2016, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
require.config({
    paths: {
        "FL@bookmarks": "src/FL@bookmarks"
        // @build (keep line)
    }
});

define(["broker", "FL@bookmarks/controller"], function (Broker, Controller) {

    "use strict";

    var API;

    Broker.commands.setHandler("FL@bookmarks:startBookmarks", function (data) {
        API.startBookmarks(data);
    });

    API = {

        startBookmarks: function (data) {
            Controller.startBookmarks(data);
        }
    };
});