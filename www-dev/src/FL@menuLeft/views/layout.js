/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2016, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
define(["marionette", "hybreed", "FL@menuLeft/config"], function (Marionette, Hybreed, Config) {

    "use strict";

    var LayoutViewNews = Marionette.LayoutView.extend({

        template: _.template(Config.html_layout),

        tagName: "div",

        className: "layout",

        regions: {
            contentCenter: "#contentCenter"
        },

        ui: {
            containerLeftMenu: ".containerLeftMenu",
            snapDrawerLeft: ".snap-drawer-left",
            optMenuLeft: "#optMenuLeft",
            optSearchRight: "#optSearchRight",
            optBack: "#optBack",
            background: ".background",
            leftMenu: "#leftMenu",

            // Menu Left
            "optUserName": "#optUserName",
            "optLogin": "#optLogin",
            "optHome": "#optHome",
            "optNews": "#optNews",
            "optBookmarks": "#optBookmarks",
            "optDownloads": "#optDownloads",
            "optBasicLegislation": "#optBasicLegislation",
            "optItemLaw": ".itemLawMenuLeft"
        },

        events: {
            "click @ui.optMenuLeft": "slideOut",
            "click @ui.leftMenu": "closeMenuLeft",
            "click @ui.optItemLaw": "viewLaw"
        },

        triggers: function () {
            var triggers = {},
                event = Hybreed.UI.getEventByPlatform();

            triggers[event + " @ui.optUserName"] = "menuLeft:click:login";
            triggers[event + " @ui.optLogin"] = "menuLeft:click:login";
            triggers[event + " @ui.optHome"] = "menuLeft:click:home";
            triggers[event + " @ui.optNews"] = "menuLeft:click:news";
            triggers[event + " @ui.optBookmarks"] = "menuLeft:click:bookmarks";
            triggers[event + " @ui.optDownloads"] = "menuLeft:click:downloads";
            triggers[event + " @ui.optBasicLegislation"] = "menuLeft:click:basicLegislation";


            return triggers;
        },

        initialize: function (options) {
            this.snapper = null;
            this.strings = options.strings;
            this.collection = options.collection;
        },

        serializeData: function () {
            return {
                items: this.collection.toJSON(),
                strings: this.strings
            };
        },

        onDestroy: function () {
            window.onresize = null;

            if (!_.isNull(this.scroll)) {
                this.scroll.destroy();
                this.scroll = null;
            }
        },

        onShow: function () {
            var that = this;

            this.snapper = new Snap({
                element: this.ui.snapDrawerLeft[0],
                disable: 'right',
                tapToClose: false,
                hyperextensible: false,
                maxPosition: this.ui.snapDrawerLeft.width()
            });

            this.snapper.on("animated", function () {

                if (_.isEqual(that.snapper.state().state, "left")) {
                    that.ui.optMenuLeft.addClass("backButton");
                    that.ui.background.fadeIn(100);

                } else {
                    that.ui.optMenuLeft.removeClass("backButton");
                    that.ui.background.fadeOut(100);
                }
            });

            window.onresize = function () {
                setTimeout(function () {

                    var state = that.snapper.state().state;

                    that.ui.snapDrawerLeft.removeAttr("style");

                    that.snapper.settings({
                        element: that.ui.snapDrawerLeft[0],
                        disable: 'right',
                        tapToClose: false,
                        hyperextensible: false,
                        maxPosition: that.ui.snapDrawerLeft.width()
                    });

                    if (_.isEqual(state, "left")) {
                        that.snapper.open('left');
                    } else {
                        that.snapper.close();
                    }

                }, 600);
            };

            this.scroll = Hybreed.UI.generateScroll(this.ui.containerLeftMenu[0], null);
        },

        slideOut: function () {
            if (_.isEqual(this.snapper.state().state, "left")) {
                this.snapper.close();
            } else {
                this.snapper.expand('left');
            }
        },

        closeMenuLeft: function(ev) {
            var $element = $(ev.target);

            if(!$element.hasClass("containerLeftMenu") && $element.parents(".containerLeftMenu").length == 0) {
                this.snapper.close();
            }
        },

        viewLaw: function(ev) {
            var lawId = $(ev.currentTarget).attr("data-id");

            this.trigger("menuLeft:click:law", this.collection.get(lawId));
        }
    });

    return LayoutViewNews;

});