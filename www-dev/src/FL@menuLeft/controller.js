/*
 * Hybreed Mobile Application View Module
 *
 * This file is part of the Hybreed package.
 * @license Hybreed Copyright (c) 2010-2015, Aplicaciones y Tratamientos de Sistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Configure the Application View Module for Sample
 */
define(["hybreed", "broker", "FL@menuLeft/views/layout", "text!data/translations.json", "text!data/mainLaws.json"],
    function (Hybreed, Broker, LayoutView, Strings, MainLaws) {

        "use strict";

        var API,
            PrivateAPI;

        // VIEWS
        var layoutView = null,
            strings = JSON.parse(Strings);

        // COLLECTIONS
        var mainLawsCollection = null;


        /**
         *     *************  PUBLIC API  ************
         */

        API = {

            generateMenuLeft: function () {

                if (_.isNull(mainLawsCollection)) {
                    PrivateAPI.startMainLawsCollection();
                }

                layoutView = new LayoutView({collection: mainLawsCollection, strings: strings});

                PrivateAPI.associateEventsLayout();

                return layoutView;
            }

        };


        /**
         *     *************   PRIVATE API  ************
         */
        PrivateAPI = {

            startMainLawsCollection: function () {
                mainLawsCollection = new Backbone.Collection(JSON.parse(MainLaws), {});
            },

            associateEventsLayout: function () {
                var that = this;

                layoutView.getRegion("contentCenter").on("show", function (view) {

                    view.on("menuLeft:slideOut", function () {
                        layoutView.slideOut();
                    });

                });

                layoutView.on("menuLeft:click:login", function () {
                    Broker.commands.execute("FL@login:showLoginView");
                });

                layoutView.on("menuLeft:click:home", function () {
                    Broker.commands.execute("FL@home:startHome");
                });

                layoutView.on("menuLeft:click:news", function () {
                    Broker.commands.execute("FL@news:startNews");
                });

                layoutView.on("menuLeft:click:bookmarks", function () {
                    Broker.commands.execute("FL@bookmarks:startBookmarks");
                });

                layoutView.on("menuLeft:click:downloads", function () {
                    Broker.commands.execute("FL@home:startDownloads");
                });

                layoutView.on("menuLeft:click:basicLegislation", function() {
                    Broker.commands.execute("FL@home:startHome", "basicLegislation");
                });

                layoutView.on("menuLeft:click:law", function(model) {
                    Broker.commands.execute("FL@law:startLaw", model);
                });
            }

        };

        return API;

    });