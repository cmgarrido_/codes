define(["crypto", "pbkdf2-min"],
    function () {

        "use strict";
        return {
            crypt: CryptoJS,

            iv: "F27D5C9927726BCEFE7510B1BDD3D137",
            salt: "3FF2EC019C627B945225DEBAD71A01B6985FE84C95A70EB132882F88C0A59A55",
            passPhrase: "9943ea07daf0eec3a29fb716e61e4480", //md5("EditorialEFL");

            generateKey: function () {
                var key = this.crypt.PBKDF2(
                    this.passPhrase,
                    this.crypt.enc.Hex.parse(this.salt),
                    {keySize: 128 / 32, iterations: 1});
                return key;
            },

            decrypt: function (text2Decrypt) {
                var key = this.generateKey(this.passPhrase),
                    cipherParams = this.crypt.lib.CipherParams.create({
                        ciphertext: this.crypt.enc.Base64.parse(text2Decrypt)
                    }),
                    decrypted = this.crypt.AES.decrypt(
                        cipherParams,
                        key,
                        {iv: this.crypt.enc.Hex.parse(this.iv)});
                return decrypted.toString(this.crypt.enc.Utf8);
            },

            encrypt: function (text2Encrypt) {
                var key = this.generateKey(this.passPhrase),
                    encrypted = this.crypt.AES.encrypt(
                        text2Encrypt,
                        key,
                        {iv: this.crypt.enc.Hex.parse(this.iv)});
                return encrypted.ciphertext.toString(this.crypt.enc.Base64);
            }
        }
    });