/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2014, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
define(["hybreed", "backbone", "spin", "progressbar", "iscroll", "iscroll-infinite", "iscroll-probe", "snap", "moment", "jquery-scrollTo"],
    function (Hybreed, Backbone, Spinner) {


        Hybreed.UI = {

            progressbar: null,

            progressByIndex: [],

            events: _.extend({}, Backbone.Events),

            defaultUIOptions: {
                bounce: true,
                mouseWheel: true,
                probeType: 1,
                click: true,
                tap: true,
                preventDefaultException: {tagName: /^(SECTION)$/}
            },

            ui: {
                $spinner: $("#spinner"),
                $wspinner: $(".wrapper_spinner"),
                $progressbar: $("#progressbar"),
                $wprogressbar: $(".wrapper_progressbar")
            },

            generateScroll: function (selector, options) {
                var scroll = null;

                if (Hybreed.mobile && Hybreed.platform !== "ios") {
                    $(selector).css("overflow", "auto");
                } else {
                    $(selector).css("overflow", "hidden");
                    if (_.isNull(options)) {
                        scroll = new IScroll(selector, this.defaultUIOptions);
                    } else {
                        scroll = new IScroll(selector, options);
                    }
                }

                return scroll;
            },

            generateScrollInfinite: function (selector, options, infiniteElements, infiniteLimit, dataset, dataFiller) {
                var scroll = null;

                $(selector).css("overflow", "hidden");

                if (_.isUndefined(options) || _.isNull(options)) {
                    var defaultOptions = {
                        useTransition: true,
                        HWCompositing: true,
                        resizePolling: 100,
                        bounce: false,
                        probeType: 1,
                        click: true,
                        tap: true,
                        cacheSize: 100,
                        infiniteElements: infiniteElements,
                        infiniteLimit: infiniteLimit,
                        dataset: dataset,
                        dataFiller: dataFiller
                    };

                    scroll = new IScrollInfinite(selector, defaultOptions);
                } else {
                    scroll = new IScrollInfinite(selector, options);
                }

                $(selector).css({
                    "-webkit-transform": "translateZ(0)",
                    "-o-transform": "translateZ(0)",
                    "-moz-transform": "translateZ(0)",
                    "transform": "translateZ(0)"
                });

                return scroll;
            },


            generateScrollProbe: function (selector, options) {
                var scroll = null;

                if (Hybreed.mobile && Hybreed.platform !== "ios") {
                    $(selector).css("overflow", "auto");
                } else {
                    $(selector).css("overflow", "hidden");

                    if (_.isUndefined(options) || _.isNull(options)) {

                        scroll = new IScrollProbe(selector, this.defaultUIOptions);
                    } else {
                        scroll = new IScrollProbe(selector, options);
                    }

                    $(selector).css({
                        "-webkit-transform": "translateZ(0)",
                        "-o-transform": "translateZ(0)",
                        "-moz-transform": "translateZ(0)",
                        "transform": "translateZ(0)"
                    });
                }

                return scroll;
            },

            generateScrollAllPlatforms: function (selector, options) {
                var scroll = null;

                $(selector).css("overflow", "hidden");

                if (_.isNull(options)) {
                    scroll = new IScroll(selector, this.defaultUIOptions);
                } else {
                    scroll = new IScroll(selector, options);
                }

                return scroll;
            },

            createSpinner: function (target) {
                var opts = {
                    lines: 13,
                    length: 9,
                    width: 3,
                    radius: 12,
                    corners: 1.0,
                    rotate: 0,
                    trail: 60,
                    speed: 1.0,
                    direction: 1,
                    shadow: true,
                    hwaccel: true,
                    color: '#fff'
                };

                var spinner = new Spinner(opts).spin(target);

                if (!this.ui.$spinner.hasClass("spinner"))
                    this.ui.$spinner.addClass("spinner");
            },

            showSpinner: function (text) {

                $(".innerTextSpinner").remove();

                if (!_.isNull(text) && !_.isEmpty(text)) {

                    var content = text.split(" ");


                    this.ui.$spinner.addClass("borderRounded");

                    //$(".spinner.borderRounded").css("width", 100 * numWords + 'px');
                    //$(".spinner.borderRounded").css("height", 100 * numWords + 'px');

                    $(".spinner.borderRounded").css("width", '200px');
                    $(".spinner.borderRounded").css("height", '200px');

                    this.ui.$spinner.append('<div class="innerTextSpinner">' + text + '</div>');
                } else {

                    this.ui.$spinner.removeClass("borderRounded");
                }

                this.ui.$wspinner.show();

            },

            showSpinnerAndWait: function (time, text) {
                var deferred = $.Deferred();
                time = time || 50;

                this.showSpinner(text);
                setTimeout(function () {
                    deferred.resolve();
                }, time);

                return deferred.promise();
            },

            hideSpinner: function () {
                this.ui.$wspinner.hide();
            },

            createProgressBar: function () {
                // Initialice
                this.progressBar = new ProgressBar("progressbar-control", {'width': '100%', 'height': '15px'});
            },

            getProgressBar: function () {
                return this.progressBar;
            },

            setPercentProgressBarByIndex: function (index, percent) {
                if (this.ui.$progressbar.is(':visible')) {
                    this.progressByIndex[index] = percent;
                    this.progressBar.setPercent(_.reduce(this.progressByIndex, function (memo, num) {
                        return memo + num;
                    }, 0));
                }
            },

            setPercentProgressBar: function (percent) {
                this.progressBar.setPercent(percent);
            },

            showProgressBar: function (text, typeCancel, cancelText, processText) {
                var that = this;

                this.processText = processText;

                $(".innerText").remove();
                $(".cancelButton").remove();
                $(".textBottom").remove();

                if (!_.isNull(text) && !_.isEmpty(text)) {
                    this.ui.$progressbar.addClass("borderRounded");

                    $(".progressbar.borderRounded").css("width", '200px');
                    //$(".progressbar.borderRounded").css("height", '160px');

                    this.ui.$progressbar.prepend('<div class="innerText">' + text + '</div>');
                } else {
                    this.ui.$progressbar.removeClass("borderRounded");
                }

                if (!_.isUndefined(typeCancel) && !_.isNull(typeCancel) && !_.isEmpty(cancelText)) {
                    this.ui.$progressbar.addClass("cancelAction" + typeCancel);
                    if (typeCancel == 1) {
                        this.ui.$progressbar.append('<div class="cancelButton">' + ((!_.isUndefined(cancelText) && !_.isNull(cancelText)) ? cancelText : 'Cancel') + '</div>');
                    } else {
                        this.ui.$progressbar.append('<div class="cancelButton"></div>');
                    }

                    this.ui.$progressbar.find(".cancelButton").on('click', function () {
                        that.events.trigger("cancelProgressBar");
                    });

                } else {
                    this.ui.$progressbar.removeClass("cancelAction1");
                    this.ui.$progressbar.removeClass("cancelAction2");
                }

                this.ui.$progressbar.find(".progressbar-control .item-bar.blue").css({width: "0%"});

                this.ui.$wprogressbar.show();
            },


            setTextProgressBar: function (value) {
                this.ui.$progressbar.find(".innerText").text(value);
            },

            hideCancelProgressBar: function (value) {
                if (!_.isUndefined(value)) {
                    this.progressBar.setPercent(value);
                }
                $(".cancelButton").hide();
                this.ui.$progressbar.find(".innerText").text(((!_.isUndefined(this.processText) && !_.isNull(this.processText)) ? this.processText : "Processing..."));
                this.ui.$progressbar.removeClass("cancelAction1");
                //this.ui.$progressbar.append('<div class="textBottom"><label>' + ((!_.isUndefined(this.processText) && !_.isNull(this.processText)) ? this.processText : "Processing...") + '</label></div>');
            },

            hideProgressBar: function () {
                this.ui.$wprogressbar.hide();
                this.progressBar.setPercent(0);
                this.progressByIndex = [];
            },

            getEventByPlatform: function() {
                return ((Hybreed.mobile && _.isEqual(Hybreed.platform, "ios")) ? "tap" : "click")
            }

        };
    });
