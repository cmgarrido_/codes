/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2014, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
define(["hybreed", "uuid"], function (Hybreed) {

    var categorie,
        elapsedTime,
        googleAnalytics,
        startTime,
        stopTime,
        stopwatch;

    googleAnalytics = {};
    categorie = "/AP_movil/android/AP_infantil_kurio_videos";
    stopwatch = {};

    googleAnalytics.init = function () {

        var uuid;
        Hybreed.log("[googleAnalytics] init");
        __gaTracker('create', 'UA-7007193-3'); // UA-7007193-1 (RTVE)

        if (localStorage.getItem("UUID")) {
            __gaTracker("send", "event", categorie, "status", localStorage.getItem("UUID"), 1);
            return Hybreed.log("[googleAnalytics] Enviado UUID de localStorage " + localStorage.getItem("UUID"));
        } else {
            uuid = Math.uuid();
            localStorage.setItem("UUID", uuid);
            __gaTracker("send", "event", categorie, "status", uuid, 1);
            return Hybreed.log("[googleAnalytics] Enviado UUID creado " + uuid);
        }
    };

    googleAnalytics.sendView = function (view) {
        __gaTracker('send', view.toUpperCase());
        return Hybreed.log("[googleAnalytics] View send " + view.toUpperCase());
    };

    googleAnalytics.sendEvent = function (event, id) {
        __gaTracker('send', 'event', categorie, event, id, 1);
        return Hybreed.log("[googleAnalytics] Event send " + event);
    };

    startTime = function (name) {
        var now;
        now = Date().getTime();
        stopwatch[name] = now;
        return Hybreed.log("[googleAnalytics] Start \t" + name);
    };

    elapsedTime = function (name) {
        var elapsed, now;
        now = Date().getTime();
        elapsed = (now - stopwatch[name]) / 1000;
        elapsed = elapsed.toFixed(4);
        Hybreed.log("[googleAnalytics] Elapsed  \t" + name + " " + elapsed + "s");
        return elapsed;
    };

    stopTime = function (name) {
        return Hybreed.log("[googleAnalytics] Stop  \t" + name + " " + getTime(name) + "s");
    };

    return googleAnalytics;
});

