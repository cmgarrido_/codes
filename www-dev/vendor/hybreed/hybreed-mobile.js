/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2014, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
require(["domReady", "hybreed", "config.prod", "application"],
    function (domReady, Hybreed, config) {

        /*
         * domReady is RequireJS plugin that triggers when DOM is ready
         */
        domReady(function () {

            var onDeviceReady = function (desktop) {


                Hybreed.init(true);

                if (Hybreed.mobile === true) {

                    if ( typeof navigator.splashscreen != "undefined" ) {
                        navigator.splashscreen.hide();
                    }

                    Hybreed.checkNet();
                }

                Hybreed.log("[Hybreed Mobile] Init");
                var application = new Hybreed.Application(config);
                application.init();
                application.start();

            };

            Hybreed.getPlatform();

            if (Hybreed.mobile === true) {
                document.addEventListener("online", Hybreed.checkNet, false); // Cordova
                document.addEventListener("offline", Hybreed.checkNet, false); // Cordova
                document.addEventListener("deviceready", onDeviceReady, false); // This
            }
            else {
                // Polyfill for navigator.notification features to work in browser when debugging
                navigator.notification = {
                    alert: function (message) {
                        return alert(message);
                    }
                };
                return onDeviceReady(true);
            }

        });


    });