/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2014, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
define(["hybreed", "jsencrypt"],
    function (Hybreed) {

        Hybreed.Crypt = {
            crypt: new JSEncrypt(),

            setPublicKey: function (key) {
                return this.crypt.setPublicKey(key);
            },

            encrypt: function (text) {
                return  this.crypt.encrypt(text);
            },

            decrypt: function (text) {
                return this.crypt.decrypt(text);
            }
        };
    });

