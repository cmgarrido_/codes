/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2014, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
define(["backbone.wreqr"], function (Wreqr) {
    return {
        commands: new Wreqr.Commands(),
        reqres: new Wreqr.RequestResponse(),
        events: new Wreqr.EventAggregator(),

        showViewInContent: function (view, content, preventDestroy, animationType) {
            preventDestroy = preventDestroy || false;
            this.events.trigger("app:show:" + content, view, preventDestroy);
            if (!_.isUndefined(animationType)) {
                view.$el.one("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function () {
                    $(this).removeClass("animated " + animationType);
                }).addClass("animated " + animationType);
            }
        },

        showViewInLayout: function (view, layout, preventDestroy, animationType) {
            preventDestroy = preventDestroy || false;
            layout.show(view, {preventDestroy: preventDestroy});
            if (!_.isUndefined(animationType)) {
                view.$el.one("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function () {
                    $(this).removeClass("animated " + animationType);
                }).addClass("animated " + animationType);
            }
        },

        showViewInContentRegionAnimation: function (view, content, animationType) {
            this.events.trigger("app:show:" + content, view, true, animationType);
        },

        showViewInLayoutRegionAnimation: function (view, layout, animationType) {
            layout.show(view, {preventDestroy: true, animation: animationType});
        }
    };
});
