/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2016, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
define(["marionette"],
    function (Marionette) {

        return Marionette.Region.extend({

            show: function (view, options) {
                if (!this._ensureElement()) {
                    return;
                }

                this._ensureViewIsIntact(view);
                Marionette.MonitorDOMRefresh(view);

                var showOptions = options || {};
                var isDifferentView = view !== this.currentView;
                var preventDestroy = !!showOptions.preventDestroy && !_.isUndefined(showOptions.animation);
                var forceShow = !!showOptions.forceShow;

                // We are only changing the view if there is a current view to change to begin with
                var isChangingView = !!this.currentView;

                // Only destroy the current view if we don't want to `preventDestroy` and if
                // the view given in the first argument is different than `currentView`
                var _shouldDestroyView = isDifferentView && !preventDestroy;

                // Only show the view given in the first argument if it is different than
                // the current view or if we want to re-show the view. Note that if
                // `_shouldDestroyView` is true, then `_shouldShowView` is also necessarily true.
                var _shouldShowView = isDifferentView || forceShow;

                if (isChangingView) {
                    this.triggerMethod('before:swapOut', this.currentView, this, options);
                }

                if (this.currentView) {
                    delete this.currentView._parent;
                }

                if (_shouldDestroyView) {
                    this.empty();

                    // A `destroy` event is attached to the clean up manually removed views.
                    // We need to detach this event when a new view is going to be shown as it
                    // is no longer relevant.
                } else if (isChangingView && _shouldShowView) {
                    this.currentView.off('destroy', this.empty, this);
                }

                if (_shouldShowView) {

                    // We need to listen for if a view is destroyed
                    // in a way other than through the region.
                    // If this happens we need to remove the reference
                    // to the currentView since once a view has been destroyed
                    // we can not reuse it.
                    view.once('destroy', this.empty, this);
                    view.render();

                    view._parent = this;

                    if (isChangingView) {
                        this.triggerMethod('before:swap', view, this, options);
                    }

                    this.triggerMethod('before:show', view, this, options);
                    Marionette.triggerMethodOn(view, 'before:show', view, this, options);

                    if (isChangingView) {
                        this.triggerMethod('swapOut', this.currentView, this, options);
                    }

                    // An array of views that we're about to display
                    var attachedRegion = Marionette.isNodeAttached(this.el);

                    // The views that we're about to attach to the document
                    // It's important that we prevent _getNestedViews from being executed unnecessarily
                    // as it's a potentially-slow method
                    var displayedViews = [];

                    var attachOptions = _.extend({
                        triggerBeforeAttach: this.triggerBeforeAttach,
                        triggerAttach: this.triggerAttach
                    }, showOptions);

                    if (attachedRegion && attachOptions.triggerBeforeAttach) {
                        displayedViews = this._displayedViews(view);
                        this._triggerAttach(displayedViews, 'before:');
                    }

                    if (_shouldDestroyView) {
                        this.attachHtml(view);
                        this.currentView = view;
                    } else {

                        var previousView = this.currentView;

                        this.$el.append(view.$el.css({"visibility": "hidden"}));

                        this.currentView = view;

                        view.$el.css({"visibility": "visible", "z-index": 100});

                        view.$el.one("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function () {
                            $(this).removeClass("animated " + showOptions.animation);
                            view.$el.removeAttr("style");

                            if (!_.isUndefined(previousView)) {
                                previousView.destroy();
                            }
                        }).addClass("animated " + showOptions.animation);
                    }

                    if (attachedRegion && attachOptions.triggerAttach) {
                        displayedViews = this._displayedViews(view);
                        this._triggerAttach(displayedViews);
                    }

                    if (isChangingView) {
                        this.triggerMethod('swap', view, this, options);
                    }

                    this.triggerMethod('show', view, this, options);
                    Marionette.triggerMethodOn(view, 'show', view, this, options);

                    return this;
                }

                return this;

            }

        });


    });