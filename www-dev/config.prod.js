/*
 *
 * This file is part of the Hybreed package.
 * @license Copyright (c) 2010-2016, atSistemas S.A. All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

/**
 * All modules must be defined in the same line
 * "src/module/a", "src/module/b", "src/module/c"...
 */
define(["src/FL@main/main", "src/FL@login/login", "src/FL@menuLeft/menuLeft", "src/FL@home/home", "src/FL@news/news", "src/FL@bookmarks/bookmarks", "src/FL@law/law"], /*@module_insertion_point*/
    function () {
        "use strict";
        return {
            /**
             * Object Container App regions.
             * Divs must be defined at index.html
             * <div id="<region>"></div>
             */
            "regions": {
                "header": "#header",
                "content": "#content",
                "tabbar": "#tabbar",
                "dialog": "#dialog"
            },
            /**
             * Broker commands.
             * It's triggers on startup to launch mediator modules.
             */
            "init": {
                "commands": [
                    "FL@main:startModule"
                ]
            }
        };
    });