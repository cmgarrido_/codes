#!/usr/bin/env node
var path = require("path"),
    xcode = require("xcode"),
    fs = require("fs"),
    shell = require("shelljs"),
    glob = require('glob'),
    rootdir = ".",

    iosroot = rootdir + "/platforms/ios/";


var projectName = function (dir, done) {
    glob(dir + '*.xcodeproj', function (er, files) {
        var name = path.basename(files[0], '.xcodeproj');
        done(name);
    });
};

function propReplace(obj, prop, value) {
    var res = false;
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            if (typeof obj[p] == 'object') {
                propReplace(obj[p], prop, value);
            } else if (p == prop) {
                res = true;
                obj[p] = value;
            }
        } else {
            console.log("it does not have this property");
        }
    }

    if (!res){
        obj[prop] = value;
    }

}

if (fs.existsSync(iosroot)) {

    console.log("----------------------------");
    console.log("EXECUTING XCODE7 ISSUES HOOK");
    console.log("----------------------------");

    projectName(iosroot, function (name) {

        var projectPath = iosroot + name,
            resourcesPath = projectPath + "/Resources/",
            pbxPath = iosroot + name + ".xcodeproj/project.pbxproj",
            xcodeProject = xcode.project(pbxPath);

        xcodeProject.parse(function(err){
            if (err) {
                console.log("Cannot read properties");
                //console.log('Error: ' + JSON.stringify(err));
            } else {
                var buildConfig = xcodeProject.pbxXCBuildConfigurationSection();

                propReplace(buildConfig, 'ENABLE_BITCODE', 'NO');

                fs.writeFileSync(pbxPath, xcodeProject.writeSync(), 'utf-8');
            }
        });
    });
}