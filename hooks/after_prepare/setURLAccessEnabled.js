#!/usr/bin/env node

var fs = require('fs'),
    path = require('path'),
    glob = require('glob'),
    rootdir = ".",
    iosroot = rootdir + "/platforms/ios/";


function replace_string_in_file(filename, to_replace, replace_with) {
    var data = fs.readFileSync(filename, 'utf8');

    if (data.indexOf(replace_with) > -1) {
        console.log('File already contains required text. Nothing to do.');
    } else {
        var result = data.replace(new RegExp(to_replace, "g"), replace_with);
        fs.writeFileSync(filename, result, 'utf8');
        console.log('File modified successfully');
    }

}


var projectName = function (dir, done) {
    glob(dir + '*.xcodeproj', function (er, files) {
        var name = path.basename(files[0], '.xcodeproj');
        done(name);
    });
};


var targetFile = '../platforms/ios/';

try {
    if (fs.existsSync(iosroot)) {
        projectName(iosroot, function (name) {
            var projectPath = iosroot + name,
                plistName = name + "-Info.plist",
                plistFile = projectPath + "/" + plistName,
                string2Replace = '<key>UIRequiresFullScreen</key>\n' +
                    '<true/>' +
                    '<key>NSAppTransportSecurity</key>\n' +
                    '<dict>\n' +
                    '<key>NSAllowsArbitraryLoads</key>\n' +
                    '<true/>\n' +
                    '</dict>\n';
          
            replace_string_in_file(plistFile, '</dict>\n</plist>', string2Replace + '</dict>\n</plist>');
        });
    }

} catch (e) {
    console.log("Exception modifiying " + targetFile);
}