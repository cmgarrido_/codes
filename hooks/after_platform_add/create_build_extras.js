#!/usr/bin/env node


var fs = require('fs');
var path = require('path');
var fork = require('child_process').fork,
    shell = require("shelljs");

var dir = './platforms/android/';
if (fs.existsSync(dir)){
    fs.writeFileSync(dir + 'build-extras.gradle', 'configurations { all*.exclude group: \'com.android.support\', module: \'support-v4\'}', 'utf8');
}

