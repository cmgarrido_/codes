#!/usr/bin/env node

var fs = require('fs');
var path = require('path');
var xmlreader = require('xmlreader');

var rootdir = ".";


function replace_string_in_file(filename, to_replace, replace_with) {
    var data = fs.readFileSync(filename, 'utf8');

    if (data.indexOf(replace_with) > -1) {
        console.log('File already contains required text. Nothing to do.');
    } else {
        var result = data.replace(new RegExp(to_replace, "g"), replace_with);
        fs.writeFileSync(filename, result, 'utf8');
        console.log('File modified successfully');
    }
}

var sourceFile = 'config.xml',
    targetFile = 'www-dev/data/info.json',
    secondTargetFile = 'www/data/info.json';


try {
    xmlreader.read(fs.readFileSync(sourceFile, 'utf8'), function (err, res) {
        var version = res.widget.attributes().version;
        var date = res.widget.attributes().date;
        var envSelector = res.widget.attributes().envSelector;
        var TestFairyiOS = res.widget.attributes().TestFairyiOS;
        //replace_string_in_file(targetFile, 'version', '$1');
        replace_string_in_file(targetFile, '("version":")([^,]*)(",)', '$1' + version + '$3');
        replace_string_in_file(targetFile, '("date":")([^,]*)(")', '$1' + date + '$3');
        replace_string_in_file(targetFile, '("envSelector":")([^,]*)(")', '$1' + envSelector + '$3');
        replace_string_in_file(targetFile, '("TestFairyiOS":")([^,]*)(")', '$1' + TestFairyiOS + '$3');
        replace_string_in_file(secondTargetFile, '("version":")([^,]*)(",)', '$1' + version + '$3');
        replace_string_in_file(secondTargetFile, '("date":")([^,]*)(")', '$1' + date + '$3');
        replace_string_in_file(secondTargetFile, '("envSelector":")([^,]*)(")', '$1' + envSelector + '$3');
        replace_string_in_file(secondTargetFile, '("TestFairyiOS":")([^,]*)(")', '$1' + TestFairyiOS + '$3');

    });
} catch (e) {
    console.log("File " + targetFile + " modified successfully");
}
