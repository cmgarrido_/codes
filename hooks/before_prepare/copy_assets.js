#!/usr/bin/env node
var path = require("path"),
    fs = require("fs"),
    shell = require("shelljs"),
    glob = require('glob'),
    rootdir = ".",

    iconroot = rootdir + "/graphics/ios/icons",
    androidiconroot = rootdir + "/graphics/android/icons",
    screenroot = rootdir + "/graphics/ios/splash",
    iosroot = rootdir + "/platforms/ios",
    androidroot = rootdir + "/platforms/android";

var getProjectName = function (dir, done) {
    glob(dir + '*.xcodeproj', function (er, files) {
        var name = path.basename(files[0], '.xcodeproj');
        console.log(name);
        done(name);
    });
};


if (fs.existsSync(iosroot)) {

    getProjectName('./platforms/ios/', function (name) {

        var projectname = name.replace(" ", "\\ ");

        // for some reason, using shell.cp() would throw this error:
        // "cp: copy File Sync: could not write to dest file (code=ENOENT)"
        if (fs.lstatSync(iosroot).isDirectory()) {
            shell.exec("cp -Rf " + iconroot + "/*" + " " + iosroot + "/" + projectname + "/Resources/icons/");
            shell.exec("cp -Rf " + screenroot + "/*" + " " + iosroot + "/" + projectname + "/Resources/splash/");
        }

        console.log("Copied all ios assets.");


    });
}

if (fs.existsSync(androidroot)) {

    // for some reason, using shell.cp() would throw this error:
    // "cp: copy File Sync: could not write to dest file (code=ENOENT)"
    if (fs.lstatSync(androidroot).isDirectory()) {
        shell.exec("cp -Rf " + androidiconroot + "/*" + " " + androidroot + "/res/");
    }

    console.log("Copied all android assets.");
}


