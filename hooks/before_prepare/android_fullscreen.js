#!/usr/bin/env node

var fs = require('fs');
var path = require('path');

var rootdir = ".",
    androidroot = rootdir + "/platforms/android";


function replace_string_in_file(filename, to_replace, replace_with) {
    var data = fs.readFileSync(filename, 'utf8');

    if (data.indexOf(replace_with) > -1) {
        console.log('File already contains required text. Nothing to do.');
    } else {
        var result = data.replace(new RegExp(to_replace, "g"), replace_with);
        fs.writeFileSync(filename, result, 'utf8');
        console.log('File modified successfully');
    }
}

if (fs.existsSync(androidroot)) {
    var targetFile = androidroot + '/AndroidManifest.xml';
    try {
        replace_string_in_file(targetFile, 'android:windowSoftInputMode="adjustResize"', 'android:windowSoftInputMode="adjustPan"');
    } catch (e) {
        console.log("File " + targetFile + " modified successfully");
    }
}
