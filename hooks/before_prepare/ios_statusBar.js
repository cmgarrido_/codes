#!/usr/bin/env node

var fs = require('fs');
var path = require('path');
var glob = require('glob');


var projectName = function (dir, done) {
    glob(dir + '*.xcodeproj', function (er, files) {
        var name = path.basename(files[0], '.xcodeproj');
        //console.log(name);
        done(name);
    });
};

function replace_string_in_file(filename, to_replace, replace_with) {
    var data = fs.readFileSync(filename, 'utf8');

    if (data.indexOf(replace_with) > -1) {
        console.log('File already contains required text. Nothing to do.');
    } else {
        var result = data.replace(new RegExp(to_replace, "g"), replace_with);
        fs.writeFileSync(filename, result, 'utf8');
        console.log('File modified successfully');
        //console.log(result);
    }
}


var dir = './platforms/ios/';

if (fs.existsSync(dir)) {

    projectName(dir, function (name) {
        var targetFile = dir + name + '/' + name + '-Info.plist';
        var targetFile2 = dir + name + '/Classes/MainViewController.m';
        console.log('[SQLTE plugin] Modifying file ' + targetFile + ' ...');
        //replace_string_in_file(targetFile, '</dict>', '<key>UIViewControllerBasedStatusBarAppearance</key>\n<true/>\n</dict>');
        console.log('[SQLTE plugin] Modifying file ' + targetFile2 + ' ...');
        //replace_string_in_file(targetFile2, '@implementation MainViewController', '@implementation MainViewController\n\n-(UIStatusBarStyle)preferredStatusBarStyle{\nreturn UIStatusBarStyleLightContent;\n}\n\n');

    });
}