#!/usr/bin/env node

var fs = require('fs');
var path = require('path');
var fork = require('child_process').fork;

/*var rootdir = process.argv[2];

console.log(rootdir);*/

var walk = function(dir, done) {
    var results = [];
    fs.readdir(dir, function(err, list) {
        if (err) return done(err);
        var i = 0;
        (function next() {
            var file = list[i++];
            if (!file) return done(null, results);
            file = dir + '/' + file;
            fs.stat(file, function(err, stat) {
                if (stat && stat.isDirectory()) {
                        var hookFolder = file + '/hooks';
                        fs.stat(hookFolder, function (err, hookstat){
                            if (hookstat && hookstat.isDirectory()){
                                readHooks(hookFolder, function(err,res){
                                    results = results.concat(res);
                                });
                            }
                        });
                    next();
                } else {
                    next();
                }
            });
        })();
    });
};

var readHooks = function(dir, done){
    var hooks = [];
    fs.readdir(dir, function(err, list) {
        if (err) return done(err);
        var i = 0;
        (function next() {
            var file = list[i++];
            if (!file) return done(null, hooks);
            file = dir + '/' + file;
            fs.stat(file, function(err, stat) {
                if (stat && stat.isDirectory()) {

                    next();
                } else {
                    hooks.push(file);
                    next();
                }
            });
        })();
    });
};

walk("./plugins", function(err, results) {
    if (err) throw err;
    results.forEach(function(hook){
        console.log('Executing hook ' + hook);
        fork(hook);
    });
});